#!/bin/bash

### Update the system

sudo apt -y update && sudo apt -y upgrade

### Install programs

echo "INSTALLING PROGRAMS"

sudo apt install -y neofetch
sudo apt install -y gnome-tweaks
sudo apt install -y exfat-utils fuse-exfat
sudo apt install -y mlocate
sudo apt install -y htop
sudo apt install -y meld
sudo apt install -y supertuxkart
sudo apt install -y supertux
sudo apt install -y ffmpeg
sudo apt install -y popsicle
sudo apt install -y telegram-desktop
sudo apt install -y kdeconnect
sudo apt install -y gparted

echo "INSTALL OF PROGRAMS COMPLETE"
sleep 5s

# Add calibre from source

echo "INSTALLING CALIBRE FROM SOURCE"
sleep 5s

sudo apt -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin

echo "CALIBRE INSTALL COMPLETE"
sleep 5s

# INSTALL GNOME EXTENSIONS

echo "INSTALLING GNOME EXTENSIONS"
sleep 5s

sudo apt install -y gnome-shell-extension-caffeine


#THINGS FOR GAMES

echo "INSTALLING GAMING SOFTWARE"
sleep 3s

#INSTALL OF WINE SOFTWARE

sudo dkpg --add-architecture i386
wget -O - https://dl.winehq.org/wine-builds/winehq.key | sudo apt-key add -
sudo add-apt-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ focal main'
sudo apt update
sudo apt install --install-recommends winehq-staging

#INSTALL FERAL GAMEMODE

sudo apt install -y gamemode

#INSTALL STEAM

sudo apt install -y steam

#INSTALL LUTRIS

sudo add-apt-repository ppa:lutris-team/lutris
sudo apt-get update
sudo apt install -y lutris

echo "OPERATION COMPLETE"
sleep 5s

### Remove programs

echo "REMOVING UNWANTED PROGRAMS"
sleep 5s

sudo apt autoremove -y cheese
sudo apt autoremove -y rhythmbox
sudo apt autoremove -y geary
sudo apt autoremove -y brasero

echo "OPERATION COMPLETE"
sleep 5s

### Check to see if bashrc file is present. If not, it'll create it

echo "CREATING THE FILES FOR NEW THEMES AND ICONS"
sleep 3s

if [ ! -d ~/.themes ]; then
	mkdir ~/.themes;
fi

if [ ! -d ~/.icons ]; then
	mkdir ~/.icons;
fi

echo "OPERATION COMPLETE"
sleep 5s

### Amend bashrc file

echo "UPDATING BASHRC FILE"
sleep 5s

echo "" >> ~/.bashrc
echo "neofetch" >> ~/.bashrc
echo "" >> ~/.bashrc
echo "### ALIASES ###" >> ~/.bashrc
echo "" >> ~/.bashrc
echo "alias ..='cd ..'" >> ~/.bashrc
echo "alias se='search'" >> ~/.bashrc
echo "alias in='install'" >> ~/.bashrc
echo "alias rm='remove'" >> ~/.bashrc
echo "alias md='mkdir'" >> ~/.bashrc
echo "alias rd='rmdir'" >> ~/.bashrc
echo "alias l='ls -alFh --color=auto'" >> ~/.bashrc

echo "OPERATION COMPLETE"
sleep 5s

#ENABLE FIREWALL (UFW)

echo "ENABLE FIREWALL"
sleep 5s

sudo ufw enable

sudo ufw allow 1714:1764/tcp
sudo ufw allow 1714:1764/udp

sudo ufw reload

sudo ufw status

sleep 6s

echo "OPERATION COMPLETE"
sleep 5s

### Update database for locate
echo "UPDATING MLOCATE DATABASE"
sleep 5s

sudo updatedb

echo "DATABASE UPDATED"
sleep 5s

#FINAL UPDATE
echo "FINAL SYSTEM UPDATE"
sleep 5s

sudo apt -y update

echo "OPERATION COMPLETE"
sleep 3s

#REBOOT SYSTEM

echo "SYSTEM REBOOTING"
sleep 5s

reboot now
