#!/bin/bash

### Update the system

sudo apt -y update && sudo apt -y upgrade

### Install programs

echo "INSTALLING PROGRAMS"

sudo apt install -y neofetch
sudo apt install -y steam
sudo apt install -y gnome-tweaks
sudo apt install -y exfat-utils fuse-exfat
sudo apt install -y mlocate
sudo apt install -y geany geany-plugins-*
sudo apt install -y gnome-shell-theme gnome-shell-theme-yaru gnome-shell-theme-selene
sudo apt install -y htop
sudo apt install -y meld
sudo apt install -y supertuxkart
sudo apt install -y ffmpeg
sudo apt install -y vim
sudo apt install -y lutris
sudo apt install -y dxvk


echo "INSTALL OF PROGRAMS COMPLETE"
sleep 5s

### Update database for locate
echo "UPDATING MLOCATE DATABASE"

sudo updatedb

echo "DATABASE UPDATED"
sleep 5s

### Remove programs

sudo apt autoremove -y cheese rhythmbox brasero 

### Check to see if bashrc file is present. If not, it'll create it

if [[ ! -f ~/.bashrc ]]; then
	touch ~/.bashrc
fi


### Amend bashrc file

echo "" >> ~/.bashrc
echo "neofetch" >> ~/.bashrc
echo "" >> ~/.bashrc
echo "### ALIASES ###" >> ~/.bashrc
echo "" >> ~/.bashrc
echo "alias ..='cd ..'" >> ~/.bashrc
echo "alias se='search'" >> ~/.bashrc
echo "alias in='install'" >> ~/.bashrc
echo "alias rm='remove'" >> ~/.bashrc  
echo "alias md='mkdir'" >> ~/.bashrc
echo "alias rd='rmdir'" >> ~/.bashrc
echo "alias l='ls -alFh --color=auto'" >> ~/.bashrc


echo "################################################################"
echo "###################    T H E   E N D      ######################"
echo "################################################################"
